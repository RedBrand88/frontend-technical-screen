import React from "react";
import "./App.css";

interface user {
  id: number;
  name: string;
  username: string;
  email: string;
  address: {
    street: string;
    suite: string;
    city: string;
    zipcode: string;
    geo: {
      lat: string;
      lng: string;
    };
  };
}

function App() {

  return (
    <div className="App">
      change this view
    </div>
  );
}

export default App;
